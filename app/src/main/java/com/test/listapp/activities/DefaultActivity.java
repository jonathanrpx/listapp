package com.test.listapp.activities;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.cantrowitz.rxbroadcast.RxBroadcast;
import com.test.listapp.R;
import com.test.listapp.utils.DialogHelper;
import com.test.listapp.utils.Trace;
import com.test.listapp.utils.Utils;

import java.io.IOException;
import java.net.UnknownHostException;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

public abstract class DefaultActivity extends AppCompatActivity
        implements Action1<Throwable> {

    @Nullable
    @Bind(R.id.toolbar)
    protected Toolbar toolbar;

    private Subscription mSubscription, mConnectionLostSubscription;

    private boolean mVisible;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        setupToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        synchronized (DefaultActivity.this) {
            mVisible = true;
        }
        if (!(this instanceof LauncherActivity)) {
            mSubscription = connectionChecker()
                    .subscribe(connected -> {
                        if (!connected)
                            showConnectionSnackBar();
                        else
                            Utils.unsubscribeIfNotNull(mConnectionLostSubscription);
                    }, DefaultActivity.this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        synchronized (DefaultActivity.this) {
            mVisible = false;
        }

        if (!Utils.isUnsubscribed(mSubscription))
            mSubscription.unsubscribe();

    }

    private void setupToolbar() {
        if (toolbar != null)
            setSupportActionBar(toolbar);
    }

    protected void setDisplayHomeAsUpEnabled(boolean enabled) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
    }


    @Override
    public void call(Throwable throwable) {
        if (throwable instanceof UnknownHostException ||
                throwable instanceof IOException) {
            Log.e(this.getClass().getName(), "onError()", throwable);
        }
    }

    private static long mLastClickTime = 0;

    public synchronized boolean isDoubleClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000)   // In milliseconds ...
            return true;
        mLastClickTime = SystemClock.elapsedRealtime();
        return false;
    }

    public boolean isConnected() {
        ConnectivityManager service = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = service.getActiveNetworkInfo();
        Trace.trace(DefaultActivity.this, "isConnected(" + (info != null && info.isConnected() && info.isAvailable()) + ")");
        return (info != null && info.isConnected() && info.isAvailable());
    }

    public Observable<Boolean> connectionChecker() {
        return Observable.merge(
                Observable.just(isConnected()),
                RxBroadcast.fromBroadcast(DefaultActivity.this, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
                        .map(intent -> isConnected()));
    }

    public boolean isVisible() {
        return mVisible;
    }

    public int getColorResource(final int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return getColor(id);
        } else {
            return getResources().getColor(id);
        }
    }

    private void showConnectionSnackBar() {
        if (Utils.isUnsubscribed(mConnectionLostSubscription))
            mConnectionLostSubscription = DialogHelper
                    .showSnackbar(DefaultActivity.this, R.string.dlg_label_no_connection,
                            R.string.dlg_label_no_connection_action, Snackbar.LENGTH_INDEFINITE)
                    .subscribe(action -> this.startActivity(new Intent(Settings.ACTION_SETTINGS))
                            , DefaultActivity.this);
    }
}
