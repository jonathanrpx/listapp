package com.test.listapp.activities;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.test.listapp.BuildConfig;
import com.test.listapp.R;
import com.test.listapp.business.ImageBI;
import com.test.listapp.components.DaggerNetworkComponent;
import com.test.listapp.domain.AppInfo;
import com.test.listapp.modules.NetworkApiModule;
import com.test.listapp.views.adapters.ImageAdapter;

import java.text.SimpleDateFormat;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

public class SummaryAppActivity extends DefaultActivity
        implements View.OnClickListener {

    private static final String DATA_INTENT_APP_INFO = BuildConfig.APPLICATION_ID + "intent.data.APP_INFO";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    @Inject
    protected ImageBI mImageBI;

    @Bind(R.id.viewPager)
    protected ViewPager viewPager;
    @Bind(R.id.lblTitle)
    protected TextView lblTitle;
    @Bind(R.id.lblSummary)
    protected TextView lblSummary;
    @Bind(R.id.lblReleaseDate)
    protected TextView lblReleaseDate;
    @Bind(R.id.lblRights)
    protected TextView lblRights;

    private AppInfo mAppInfo;

    public static void show(final DefaultActivity activity,
                            final AppInfo appInfo) {
        activity.startActivity(new Intent(activity, SummaryAppActivity.class)
                .putExtra(DATA_INTENT_APP_INFO, appInfo));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_app);
        setDisplayHomeAsUpEnabled(true);

        DaggerNetworkComponent.builder()
                .networkApiModule(NetworkApiModule.get(SummaryAppActivity.this))
                .build()
                .inject(SummaryAppActivity.this);

        mAppInfo = (AppInfo) getIntent().getSerializableExtra(DATA_INTENT_APP_INFO);
        if (mAppInfo != null)
            viewPager.setAdapter(new ImageAdapter(SummaryAppActivity.this, mImageBI, mAppInfo.getImages()));

        lblSummary.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAppInfo == null) finish();
        else {
            setTitle(mAppInfo.getName());
            lblTitle.setText(mAppInfo.getTitle());
            lblSummary.setText(mAppInfo.getSummary());
            lblReleaseDate.setText(String.format(getString(R.string.format_release_date), DATE_FORMAT.format(mAppInfo.getReleaseDate())));
            lblRights.setText(mAppInfo.getRights());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.lblTitle)
    @Override
    public void onClick(View view) {
        if (!isDoubleClick()) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mAppInfo.getUrl()));
                startActivity(browserIntent);
            } catch (Throwable ignored) {
            }
        }
    }
}
