package com.test.listapp.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.test.listapp.R;
import com.test.listapp.utils.Utils;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;

public class LauncherActivity extends DefaultActivity {

    private Subscription mSubscription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laucher);

        mSubscription = Observable.timer(5, TimeUnit.SECONDS)
                .subscribe(t -> MainActivity.showAndFinish(LauncherActivity.this),
                        LauncherActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Utils.isUnsubscribed(mSubscription))
            mSubscription.unsubscribe();
    }
}
