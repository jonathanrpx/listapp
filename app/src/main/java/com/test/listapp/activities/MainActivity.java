package com.test.listapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.test.listapp.R;
import com.test.listapp.business.ImageBI;
import com.test.listapp.business.TopAppsBI;
import com.test.listapp.components.DaggerNetworkComponent;
import com.test.listapp.domain.AppInfo;
import com.test.listapp.domain.CategoryInfo;
import com.test.listapp.managers.AppsManager;
import com.test.listapp.managers.CategoryManager;
import com.test.listapp.modules.NetworkApiModule;
import com.test.listapp.sql.ListAppDB;
import com.test.listapp.utils.DialogHelper;
import com.test.listapp.utils.Utils;
import com.test.listapp.views.adapters.AppAdapter;

import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import butterknife.Bind;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Actions;

public class MainActivity extends DefaultActivity {

    public static final String CATEGORY_ALL = "CATEGORY_ALL";

    @Inject
    protected TopAppsBI mTopAppsApi;
    @Inject
    protected ImageBI mImageBI;

    @Bind(R.id.rvApps)
    protected RecyclerView rvApps;
    @Bind(R.id.swpLayout)
    protected SwipeRefreshLayout swpLayout;

    private AppAdapter mAppAdapter;
    private Subscription mSubscription, mFilterSubscription;
    private AtomicBoolean mCategoriesLoaded = new AtomicBoolean(false);
    private MenuItem mMenuItemFilter;

    public static void showAndFinish(DefaultActivity activity) {
        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DaggerNetworkComponent.builder()
                .networkApiModule(NetworkApiModule.get(MainActivity.this))
                .build()
                .inject(MainActivity.this);

        rvApps.setHasFixedSize(true);
        rvApps.setLayoutManager(new GridLayoutManager(MainActivity.this, Utils.calculateNoOfColumns(MainActivity.this)));
        rvApps.setAdapter(mAppAdapter = new AppAdapter(MainActivity.this, mImageBI));

        int color = getColorResource(R.color.colorPrimary);
        swpLayout.setColorSchemeColors(color, color, color, color);
        swpLayout.setEnabled(false);

        ListAppDB.clear(MainActivity.this)
                .single()
                .subscribe(Actions.empty(), MainActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSubscription == null || mSubscription.isUnsubscribed() && mAppAdapter.isEmpty())
            mSubscription = AppsManager
                    .findAll(MainActivity.this)
                    .doOnNext(apps -> {
                        mCategoriesLoaded.set(true);
                        setFilterVisible(true);
                    })
                    .onErrorResumeNext(throwable -> {
                        if (throwable instanceof NoSuchElementException)
                            return mTopAppsApi.getTopApps()
                                    .doOnNext(apps -> {
                                        AppsManager
                                                .set(MainActivity.this, apps)
                                                .subscribe(Actions.empty(), MainActivity.this);

                                        Observable.from(apps)
                                                .map(AppInfo::getCategory)
                                                .distinct()
                                                .toList()
                                                .flatMap(categories -> CategoryManager
                                                        .set(MainActivity.this, categories))
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(saved -> {
                                                    setFilterVisible(saved);
                                                    mCategoriesLoaded.set(saved);
                                                }, MainActivity.this);
                                    });
                        return Observable.error(throwable);
                    })
                    .doOnSubscribe(setSwipeRefreshing(true))
                    .doOnUnsubscribe(setSwipeRefreshing(false))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(mAppAdapter::addAll, MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.unsubscribeIfNotNull(mSubscription);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.unsubscribeIfNotNull(mFilterSubscription);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mMenuItemFilter = menu.findItem(R.id.action_categories);
        mMenuItemFilter.setVisible(!mAppAdapter.isEmpty());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (!isDoubleClick() && item.getItemId() == R.id.action_categories) {
            Utils.unsubscribeIfNotNull(mFilterSubscription);
            mFilterSubscription = CategoryManager.findAll(MainActivity.this)
                    .flatMap(categories -> {
                        categories.add(0, new CategoryInfo(CATEGORY_ALL, getString(R.string.dlg_label_all_categories), null));
                        return DialogHelper
                                .showSelectorDialog(MainActivity.this, R.string.dlg_title_categories, true,
                                        new ArrayAdapter<CategoryInfo>(MainActivity.this, R.layout.row_simple_text, categories) {

                                            @Override
                                            public View getView(final int position,
                                                                final View convertView,
                                                                @NonNull
                                                                final ViewGroup parent) {
                                                TextView textView = (TextView) super.getView(position, convertView, parent);
                                                textView.setText(categories.get(position).getName());
                                                return textView;
                                            }
                                        }, DialogHelper.NO_BUTTON);
                    })
                    .flatMap(category -> (CATEGORY_ALL.equalsIgnoreCase(category.getId()) ?
                            AppsManager.findAll(MainActivity.this) :
                            AppsManager.findByCategory(MainActivity.this, category))
                            .doOnSubscribe(setSwipeRefreshing(true))
                            .doOnUnsubscribe(setSwipeRefreshing(false))
                            .observeOn(AndroidSchedulers.mainThread()))
                    .subscribe(apps -> {
                        mAppAdapter.clear();
                        mAppAdapter.addAll(apps);
                    }, MainActivity.this);
        }
        return true;
    }

    public void selectApp(final AppInfo appInfo) {
        SummaryAppActivity.show(MainActivity.this, appInfo);
    }

    private Action0 setSwipeRefreshing(final boolean show) {
        return () -> swpLayout.post(() -> swpLayout.setRefreshing(show));
    }

    private void setFilterVisible(final boolean visible) {
        if (mMenuItemFilter != null)
            mMenuItemFilter.setVisible(visible);
    }
}
