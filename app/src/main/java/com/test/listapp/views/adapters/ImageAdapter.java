package com.test.listapp.views.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.test.listapp.R;
import com.test.listapp.activities.DefaultActivity;
import com.test.listapp.business.ImageBI;
import com.test.listapp.utils.Trace;

import java.util.List;

import rx.subscriptions.CompositeSubscription;

public class ImageAdapter extends PagerAdapter {

    private final LayoutInflater mLayoutInflater;
    private ImageBI mImageBI;
    private List<String> mImages;
    private DefaultActivity mActivity;
    private CompositeSubscription mSubscription;

    public ImageAdapter(DefaultActivity activity,
                        final ImageBI imageBI,
                        final List<String> images) {
        mImageBI = imageBI;
        mImages = images;
        mActivity = activity;
        mLayoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSubscription = new CompositeSubscription();
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout) object);
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_pager, container, false);
        container.addView(itemView);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        mSubscription.add(mImageBI
                .download(mImages.get(position), imageView)
                .subscribe(result -> Trace.trace(this, "instantiateItem(" + position + ") : " + mImages.get(position))
                        , mActivity));
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
