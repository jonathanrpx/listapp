package com.test.listapp.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.listapp.R;
import com.test.listapp.activities.MainActivity;
import com.test.listapp.business.ImageBI;
import com.test.listapp.domain.AppInfo;
import com.test.listapp.utils.Trace;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Actions;
import rx.subscriptions.CompositeSubscription;


public final class AppAdapter extends RecyclerView.Adapter<AppAdapter.ViewHolder> {

    private DecimalFormat formatter = new DecimalFormat("######.##");
    private String mPriceFormat;

    private ImageBI mImageBI;
    private MainActivity mActivity;
    private List<AppInfo> mAppItems = new ArrayList<>();

    private CompositeSubscription mSubscription;

    public AppAdapter(@NonNull
                      final MainActivity mActivity,
                      final ImageBI imageBI) {
        this.mActivity = mActivity;
        this.mSubscription = new CompositeSubscription();
        this.mPriceFormat = mActivity.getString(R.string.format_price);
        this.mImageBI = imageBI;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Lifecycle RecyclerView.Adapter Methods ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_app, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            AppInfo app = mAppItems.get(position);
            holder.lblName.setText(app.getName());
            holder.lblPrice.setText(String.format(mPriceFormat
                    , formatter.format(app.getPrice().getAmount()), app.getPrice().getCurrency()));
            mSubscription.add(mImageBI.download(app.getImages().get(0), holder.imgPicture, true)
                    .subscribe(Actions.empty(), mActivity));
        } catch (Throwable ignored) {
//            trace(this, "onBindViewHolder(position=" + position + ")...", ignored);
        }
    }

    @Override
    public int getItemCount() {
        return mAppItems.size();
    }

    public boolean isEmpty() {
        return mAppItems.isEmpty();
    }

    public void addAll(List<AppInfo> items) {
        mAppItems.addAll(items);
        notifyDataSetChanged();
    }

    public void clear(){
        mAppItems.clear();
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // ViewHolder Implementation ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    public final class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        @Bind(R.id.imgPicture)
        protected ImageView imgPicture;

        @Bind(R.id.lblName)
        protected TextView lblName;
        @Bind(R.id.lblPrice)
        protected TextView lblPrice;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // View.OnClickListener ...
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        @Override
        @OnClick(R.id.viewItem)
        public void onClick(View v) {
            if (!mActivity.isDoubleClick()) {
                try {
                    mActivity.selectApp(mAppItems.get(getAdapterPosition()));
                } catch (Throwable ignored) {
                    Trace.trace(this, "onClick(view=" + v.getId() + ")...", ignored);
                }
            }
        }
    }
}
