package com.test.listapp.modules;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.test.listapp.BuildConfig;
import com.test.listapp.business.ImageBI;
import com.test.listapp.business.TopAppsBI;
import com.test.listapp.services.ImageApi;
import com.test.listapp.services.TopAppsApi;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class NetworkApiModule {

    private Context context;

    private NetworkApiModule(Context context) {
        this.context = context;
    }

    public static NetworkApiModule get(Context context) {
        return new NetworkApiModule(context);
    }

    @Provides
    @Singleton
    public OkHttpClient getHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            builder.addInterceptor(logging);
        }
        return builder.build();
    }

    @Provides
    @Inject
    @Singleton
    public Picasso getPicasso(final OkHttpClient okHttpClient) {
        try {
            Picasso.setSingletonInstance(new Picasso
                    .Builder(context)
                    .downloader(new OkHttp3Downloader(okHttpClient))
                    .indicatorsEnabled(BuildConfig.DEBUG)
                    .build());
        } catch (Throwable ignored) {
        }
        return Picasso.with(context);
    }

    @Provides
    @Singleton
    @Inject
    public TopAppsBI getTopAppsApi(final OkHttpClient okHttpClient) {
        return new TopAppsApi("https://itunes.apple.com/us/rss/", okHttpClient);
    }

    @Provides
    @Inject
    public ImageBI getImageApi(final Picasso picasso) {
        return new ImageApi(picasso);
    }

}
