package com.test.listapp.utils;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.test.listapp.R;
import com.test.listapp.activities.DefaultActivity;
import com.test.listapp.domain.CategoryInfo;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;


public final class DialogHelper {

    public static final int NO_TITLE = -1;
    public static final int NO_BUTTON = -1;
    public static final int NO_ACTION = -1;


    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Show Selector Dialogs ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @NonNull
    public static <T> Observable<T> showSelectorDialog(@NonNull
                                                       final DefaultActivity activity,
                                                       final int titleId,
                                                       final boolean cancelable,
                                                       @NonNull
                                                       final ArrayAdapter<T> adapter,
                                                       final int positiveId) {
        return Observable
                .create((Subscriber<? super T> subscriber) -> {
                    try {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                                .setTitle(titleId == NO_TITLE ? null : activity.getString(titleId))
                                .setCancelable(cancelable)
                                .setAdapter(adapter,
                                        (dialog, which) -> {
                                            subscriber.onNext(adapter.getItem(which));
                                            subscriber.onCompleted();
                                        });
                        if (positiveId != NO_BUTTON)
                            builder.setPositiveButton(positiveId,
                                    (dialog, which) -> subscriber.unsubscribe())
                                    .create();
                        AlertDialog alertDialog = builder.create();

                        subscriber.add(Subscriptions.create(dismiss(alertDialog)));

                        if (!activity.isFinishing() && activity.isVisible())
                            activity.runOnUiThread(show(alertDialog));
                        else if (!subscriber.isUnsubscribed())
                            subscriber.unsubscribe();

                    } catch (Throwable th) {
                        subscriber.onError(th);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread());
    }


    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Show Snack Bars ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @NonNull
    public static Observable<Void> showSnackbar(@NonNull
                                                final DefaultActivity activity,
                                                final int messageId,
                                                final int actionId,
                                                final int duration) {
        return Observable
                .create((Subscriber<? super Void> subscriber) -> {
                    try {
                        Snackbar snackbar = Snackbar
                                .make(activity.findViewById(android.R.id.content),
                                        messageId,
                                        duration == Snackbar.LENGTH_INDEFINITE ? Snackbar.LENGTH_INDEFINITE : duration);

                        if (actionId != NO_ACTION)
                            snackbar.setAction(actionId, view -> {
                                subscriber.onNext(null);
                                subscriber.onCompleted();
                            });

                        snackbar.setCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                super.onDismissed(snackbar, event);
                                subscriber.unsubscribe();
                            }
                        });

                        subscriber.add(Subscriptions.create(snackbar::dismiss));

                        if (!activity.isFinishing() && activity.isVisible()) {
                            snackbar.getView().setBackgroundColor(activity.getColorResource(R.color.colorAccent));
                            snackbar.setActionTextColor(activity.getColorResource(R.color.colorPrimaryDark));
                            snackbar.show();
                        } else
                            subscriber.unsubscribe();

                    } catch (Throwable th) {
                        subscriber.onError(th);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread());
    }


    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // RxHelper Methods ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static Runnable show(final AlertDialog alertDialog) {
        return alertDialog::show;
    }

    private static Action0 dismiss(final AlertDialog alertDialog) {
        return alertDialog::dismiss;
    }

}
