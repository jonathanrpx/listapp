package com.test.listapp.utils;


import android.content.Context;
import android.util.DisplayMetrics;

import com.test.listapp.R;

import rx.Subscription;

public class Utils {

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / (context.getResources().getDimension(R.dimen.item_app_width) / context.getResources().getDisplayMetrics().density));
    }

    public static boolean isUnsubscribed(Subscription subscription) {
        return subscription == null || subscription.isUnsubscribed();
    }

    public static void unsubscribeIfNotNull(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}
