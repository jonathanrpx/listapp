package com.test.listapp.utils;

import android.util.Log;

import com.test.listapp.BuildConfig;


public final class Trace {

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Trace Methods ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    public static void trace(final Object source, final String message) {
        if (BuildConfig.DEBUG)
            Log.i(source.getClass().getSimpleName(), message);
    }

    public static void trace(final Class clazz, final String message) {
        if (BuildConfig.DEBUG)
            Log.i(clazz.getSimpleName(), message);
    }

    public static void trace(final Object source, final String message, final Throwable throwable) {
        if (BuildConfig.DEBUG)
            Log.e(source.getClass().getSimpleName(), message, throwable);
    }

    public static void trace(final Class clazz, final String message, final Throwable throwable) {
        if (BuildConfig.DEBUG)
            Log.e(clazz.getSimpleName(), message, throwable);
    }
}
