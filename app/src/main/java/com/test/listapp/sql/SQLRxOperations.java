package com.test.listapp.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.NoSuchElementException;

import rx.Observable;
import rx.schedulers.Schedulers;

public abstract class SQLRxOperations<T> {

    private Context context;

    protected SQLRxOperations(Context context) {
        this.context = context;
    }

    protected abstract String getTableName();

    protected abstract String[] getColumns();

    protected abstract ContentValues parse(final T value);

    protected abstract T parse(final Cursor cursor);

    protected Observable<SQLiteDatabase> getWritableDatabase() {
        return Observable.just(new ListAppDB(context).getWritableDatabase())
                .subscribeOn(Schedulers.computation())
                .unsubscribeOn(Schedulers.computation());
    }

    protected Observable<SQLiteDatabase> getReadableDatabase() {
        return Observable.just(new ListAppDB(context).getReadableDatabase())
                .subscribeOn(Schedulers.computation())
                .unsubscribeOn(Schedulers.computation());
    }

    public Observable<Boolean> create(@NonNull T value) {
        return getWritableDatabase()
                .flatMap(db -> Observable
                        .create((Observable.OnSubscribe<Boolean>) subscriber -> {
                            try {
                                subscriber.onNext(db.insertWithOnConflict(getTableName(), null,
                                        parse(value), SQLiteDatabase.CONFLICT_IGNORE) > 0);
                                subscriber.onCompleted();
                            } catch (Exception e) {
                                subscriber.onError(e);
                            } finally {
                                if (db.isOpen())
                                    db.close();
                            }
                        })
                        .subscribeOn(Schedulers.computation())
                        .unsubscribeOn(Schedulers.computation()));
    }

    public Observable<Boolean> create(@NonNull
                                      final List<T> values) {
        return getWritableDatabase()
                .flatMap(db -> Observable
                        .create((Observable.OnSubscribe<Boolean>) subscriber -> {
                            try {
                                for (int i = 0; i < values.size(); i++) {
                                    db.insert(getTableName(),
                                            null,
                                            parse(values.get(i)));
                                }
                                subscriber.onNext(true);
                                subscriber.onCompleted();
                            } catch (Throwable th) {
                                subscriber.onError(th);
                            } finally {
                                if (db.isOpen())
                                    db.close();
                            }
                        })
                        .subscribeOn(Schedulers.computation())
                        .unsubscribeOn(Schedulers.computation()));
    }

    public Observable<T> read() {
        return read(null, null, null);
    }

    public Observable<T> read(final String whereClause,
                              final String[] args) {
        return read(whereClause, args, null);
    }

    public Observable<T> read(final String whereClause,
                              final String[] args,
                              final String orderBy) {
        return getReadableDatabase()
                .flatMap(db -> Observable
                        .create((Observable.OnSubscribe<T>) subscriber -> {
                            Cursor cursor = null;
                            try {
                                cursor = db.query(getTableName(),
                                        getColumns(),
                                        whereClause,
                                        args,
                                        null,
                                        null,
                                        orderBy);
                                if (cursor.moveToNext()) {
                                    do {
                                        subscriber.onNext(parse(cursor));
                                    } while (cursor.moveToNext());
                                    subscriber.onCompleted();
                                }else
                                    subscriber.onError(new NoSuchElementException());
                            } catch (Throwable th) {
                                subscriber.onError(th);
                            } finally {
                                if (cursor != null && !cursor.isClosed())
                                    cursor.close();
                                if (db.isOpen())
                                    db.close();
                            }
                        })
                        .subscribeOn(Schedulers.computation())
                        .unsubscribeOn(Schedulers.computation()));
    }

    public Observable<Integer> count(final String whereClause,
                                     final String[] args) {
        return getReadableDatabase()
                .flatMap(db -> Observable
                        .create((Observable.OnSubscribe<Integer>) subscriber -> {
                            Cursor cursor = null;
                            try {
                                cursor = db.query(getTableName(),
                                        getColumns(),
                                        whereClause,
                                        args,
                                        null,
                                        null,
                                        null);
                                subscriber.onNext(cursor.moveToNext() ? cursor.getCount() : 0);
                                subscriber.onCompleted();
                            } catch (Throwable th) {
                                subscriber.onError(th);
                            } finally {
                                if (cursor != null && !cursor.isClosed())
                                    cursor.close();
                                if (db.isOpen())
                                    db.close();
                            }
                        })
                        .subscribeOn(Schedulers.computation())
                        .unsubscribeOn(Schedulers.computation()));
    }

    public Observable<T> readOrderBy(final String orderClause) {
        return read(null, null, orderClause);
    }

    public Observable<Boolean> update(@NonNull final T value) {
        return update(value, null, null);
    }

    public Observable<Boolean> update(@NonNull
                                      final T value,
                                      final String whereClause,
                                      final String[] args) {
        return update(parse(value), whereClause, args);
    }

    public Observable<Boolean> update(@NonNull
                                      final ContentValues values,
                                      final String whereClause,
                                      final String[] args) {
        return getWritableDatabase()
                .flatMap(db -> Observable
                        .create((Observable.OnSubscribe<Boolean>) subscriber -> {
                            try {
                                subscriber.onNext(db.updateWithOnConflict(getTableName(),
                                        values,
                                        whereClause,
                                        args,
                                        SQLiteDatabase.CONFLICT_IGNORE) > 0);
                                subscriber.onCompleted();
                            } catch (Throwable th) {
                                subscriber.onError(th);
                            } finally {
                                if (db.isOpen())
                                    db.close();
                            }
                        })
                        .subscribeOn(Schedulers.computation())
                        .unsubscribeOn(Schedulers.computation()));
    }

    public Observable<Boolean> delete() {
        return delete(null, null);
    }

    public Observable<Boolean> delete(final String whereClause,
                                      final String[] args) {
        return getWritableDatabase()
                .flatMap(db -> Observable
                        .create((Observable.OnSubscribe<Boolean>) subscriber -> {
                            try {
                                subscriber.onNext(db.delete(getTableName(), whereClause, args) > 0);
                                subscriber.onCompleted();
                            } catch (Throwable th) {
                                subscriber.onError(th);
                            } finally {
                                if (db.isOpen())
                                    db.close();
                            }
                        })
                        .subscribeOn(Schedulers.computation())
                        .unsubscribeOn(Schedulers.computation()));
    }

    public Observable<Boolean> isEmpty() {
        return getReadableDatabase()
                .flatMap(db -> Observable
                        .create((Observable.OnSubscribe<Boolean>) subscriber -> {
                            Cursor cursor = null;
                            try {
                                cursor = db.rawQuery("SELECT count(*) FROM " + getTableName(), null);
                                subscriber.onNext(cursor.moveToFirst() && cursor.getInt(0) == 0);
                                subscriber.onCompleted();
                            } catch (Throwable throwable) {
                                subscriber.onError(throwable);
                            } finally {
                                if (cursor != null && !cursor.isClosed())
                                    cursor.close();
                                if (db.isOpen())
                                    db.close();
                            }
                        })
                        .subscribeOn(Schedulers.computation())
                        .unsubscribeOn(Schedulers.computation()));
    }
}
