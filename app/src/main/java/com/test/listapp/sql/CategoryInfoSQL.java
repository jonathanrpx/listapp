package com.test.listapp.sql;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.test.listapp.domain.CategoryInfo;
import com.test.listapp.utils.Trace;

public class CategoryInfoSQL extends SQLRxOperations<CategoryInfo> {

    private static final String TABLE_NAME = "categories";

    private enum COLUMNS {
        ID, NAME, SCHEME;

        static String[] getAll() {
            return new String[]{
                    ID.name(),

                    NAME.name(),
                    SCHEME.name()
            };
        }
    }

    private static final String DB_CREATE =
            "create table if not exists  " + TABLE_NAME + " ("
                    + COLUMNS.ID + " text not null primary key, "
                    + COLUMNS.NAME + " text, "
                    + COLUMNS.SCHEME + " text) ";

    static void onCreate(final SQLiteDatabase db) {
        Trace.trace(CategoryInfoSQL.class, "onCreate()...");
        db.execSQL(DB_CREATE);
    }

    static void onUpdate(final SQLiteDatabase db,
                         final int oldVersion,
                         final int newVersion) {
        Trace.trace(CategoryInfoSQL.class, "onUpdate(" + oldVersion + ", " + newVersion + ")...");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public CategoryInfoSQL(Context context) {
        super(context);
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String[] getColumns() {
        return COLUMNS.getAll();
    }

    @Override
    protected ContentValues parse(CategoryInfo categoryInfo) {
        ContentValues values = new ContentValues();
        values.put(COLUMNS.ID.name(), categoryInfo.getId());
        values.put(COLUMNS.NAME.name(), categoryInfo.getName());
        values.put(COLUMNS.SCHEME.name(), categoryInfo.getScheme());
        return values;
    }

    @Override
    protected CategoryInfo parse(Cursor cursor) {
        return new CategoryInfo.Builder()
                .setId(cursor.getString(COLUMNS.ID.ordinal()))
                .setName(cursor.getString(COLUMNS.NAME.ordinal()))
                .setScheme(cursor.getString(COLUMNS.SCHEME.ordinal()))
                .build();
    }
}
