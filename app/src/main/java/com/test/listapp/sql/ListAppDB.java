package com.test.listapp.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import rx.Observable;

public final class ListAppDB extends AbstractDB {

    private static final int DB_VERSION = 1;

    public ListAppDB(@NonNull final Context context) {
        super(context, DB_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        AppInfoSQL.onCreate(db);
        CategoryInfoSQL.onCreate(db);
    }

    @Override
    public void onUpdate(@NonNull final SQLiteDatabase db,
                         final int oldVersion,
                         final int newVersion) {
        AppInfoSQL.onUpdate(db, oldVersion, newVersion);
        CategoryInfoSQL.onUpdate(db, oldVersion, newVersion);
    }

    public static Observable<Boolean> clear(@NonNull Context context) {
        return Observable.combineLatest(
                ListAppDB.getSQLOperator(context, AppInfoSQL.class).delete(),
                ListAppDB.getSQLOperator(context, CategoryInfoSQL.class).delete(),
                (apps, categories) -> apps && categories);
    }

    public static Observable<Boolean> isEmpty(@NonNull Context context) {
        return Observable.combineLatest(
                ListAppDB.getSQLOperator(context, AppInfoSQL.class).isEmpty(),
                ListAppDB.getSQLOperator(context, CategoryInfoSQL.class).isEmpty(),
                (apps, categories) -> apps && categories);
    }
}