package com.test.listapp.sql;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.test.listapp.domain.AppInfo;
import com.test.listapp.domain.CategoryInfo;
import com.test.listapp.domain.PriceInfo;
import com.test.listapp.utils.Trace;

import java.util.Date;

import rx.Observable;

public class AppInfoSQL extends SQLRxOperations<AppInfo> {

    private static final String TABLE_NAME = "app_info";


    private enum COLUMNS {
        ID, BUNDLE_ID, NAME, TITLE, IMAGES, CONTENT_TYPE, SUMMARY, RIGHTS, URL, RELEASE_DATE, PRICE_AMOUNT, PRICE_CURRENCY, CATEGORY_ID;

        static String[] getAll() {
            return new String[]{
                    ID.name(),
                    BUNDLE_ID.name(),
                    NAME.name(),
                    TITLE.name(),
                    IMAGES.name(),
                    CONTENT_TYPE.name(),
                    SUMMARY.name(),
                    RIGHTS.name(),
                    URL.name(),
                    RELEASE_DATE.name(),
                    PRICE_AMOUNT.name(),
                    PRICE_CURRENCY.name(),
                    CATEGORY_ID.name()
            };
        }
    }

    private static final String DB_CREATE =
            "create table if not exists  " + TABLE_NAME + " ("
                    + COLUMNS.ID + " text not null primary key, "
                    + COLUMNS.BUNDLE_ID + " text not null, "
                    + COLUMNS.NAME + " text, "
                    + COLUMNS.TITLE + " text null, "
                    + COLUMNS.IMAGES + " text null, "
                    + COLUMNS.CONTENT_TYPE + " text, "
                    + COLUMNS.SUMMARY + " text, "
                    + COLUMNS.RIGHTS + " text, "
                    + COLUMNS.URL + " text, "
                    + COLUMNS.RELEASE_DATE + " datetime, "
                    + COLUMNS.PRICE_AMOUNT + " real, "
                    + COLUMNS.PRICE_CURRENCY + " text, "
                    + COLUMNS.CATEGORY_ID + " text) ";

    static void onCreate(final SQLiteDatabase db) {
        Trace.trace(AppInfoSQL.class, "onCreate()...");
        db.execSQL(DB_CREATE);
    }

    static void onUpdate(final SQLiteDatabase db,
                         final int oldVersion,
                         final int newVersion) {
        Trace.trace(AppInfoSQL.class, "onUpdate(" + oldVersion + ", " + newVersion + ")...");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public AppInfoSQL(Context context) {
        super(context);
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String[] getColumns() {
        return COLUMNS.getAll();
    }

    @Override
    protected ContentValues parse(AppInfo appInfo) {
        ContentValues values = new ContentValues();
        values.put(COLUMNS.ID.name(), appInfo.getId());
        values.put(COLUMNS.BUNDLE_ID.name(), appInfo.getBundleId());
        values.put(COLUMNS.NAME.name(), appInfo.getName());
        values.put(COLUMNS.TITLE.name(), appInfo.getTitle());
        values.put(COLUMNS.CONTENT_TYPE.name(), appInfo.getContentType());
        values.put(COLUMNS.SUMMARY.name(), appInfo.getSummary());
        values.put(COLUMNS.RIGHTS.name(), appInfo.getRights());
        values.put(COLUMNS.URL.name(), appInfo.getUrl());
        values.put(COLUMNS.RELEASE_DATE.name(), appInfo.getReleaseDate().getTime());
        values.put(COLUMNS.PRICE_AMOUNT.name(), appInfo.getPrice().getAmount());
        values.put(COLUMNS.PRICE_CURRENCY.name(), appInfo.getPrice().getCurrency());
        values.put(COLUMNS.CATEGORY_ID.name(), appInfo.getCategory().getId());
        values.put(COLUMNS.IMAGES.name(), TextUtils.join(",", appInfo.getImages()));
        return values;
    }

    @Override
    protected AppInfo parse(Cursor cursor) {
        return new AppInfo.Builder()
                .setId(cursor.getString(COLUMNS.ID.ordinal()))
                .setBundleId(cursor.getString(COLUMNS.BUNDLE_ID.ordinal()))
                .setName(cursor.getString(COLUMNS.NAME.ordinal()))
                .setTitle(cursor.getString(COLUMNS.TITLE.ordinal()))
                .setContentType(cursor.getString(COLUMNS.CONTENT_TYPE.ordinal()))
                .setSummary(cursor.getString(COLUMNS.SUMMARY.ordinal()))
                .setRights(cursor.getString(COLUMNS.RIGHTS.ordinal()))
                .setUrl(cursor.getString(COLUMNS.URL.ordinal()))
                .setReleaseDate(new Date(cursor.getLong(COLUMNS.RELEASE_DATE.ordinal())))
                .setPrice(new PriceInfo(cursor.getDouble(COLUMNS.PRICE_AMOUNT.ordinal()),
                        cursor.getString(COLUMNS.PRICE_CURRENCY.ordinal())))
                .setCategory(new CategoryInfo(cursor.getString(COLUMNS.CATEGORY_ID.ordinal())))
                .setImages(cursor.getString(COLUMNS.IMAGES.ordinal()).split(","))
                .build();
    }

    public Observable<AppInfo> findByCategory(final CategoryInfo category) {
        return read(COLUMNS.CATEGORY_ID + " = ?", new String[]{category.getId()});
    }
}
