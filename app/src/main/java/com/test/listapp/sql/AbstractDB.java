package com.test.listapp.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.test.listapp.utils.Trace;


public abstract class AbstractDB extends SQLiteOpenHelper {

    private static final String DB_NAME = "listapp.db";

    public AbstractDB(@NonNull final Context context, final int version) {
        super(context, DB_NAME, null, version);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db,
                          final int oldVersion,
                          final int newVersion) {
        onUpdate(db, oldVersion, newVersion);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db,
                            int oldVersion,
                            int newVersion) {
        onUpdate(db, oldVersion, newVersion);
    }

    public abstract void onUpdate(@NonNull final SQLiteDatabase db,
                                  final int oldVersion,
                                  final int newVersion);

    public static <T extends SQLRxOperations<?>> T getSQLOperator(@NonNull final Context context,
                                                                  @NonNull final Class<T> tClass) {
        try {
            return tClass.getDeclaredConstructor(Context.class).newInstance(context);
        } catch (Exception ex) {
            Trace.trace(AbstractDB.class, "getSQLOperator(" + tClass + ")...", ex);
            throw new RuntimeException(ex);
        }
    }
}