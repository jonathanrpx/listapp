package com.test.listapp.factories;

import org.json.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public final class CustomConverterFactory extends Converter.Factory {

    private CustomConverterFactory() {
    }

    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations,
                                                            Retrofit retrofit) {
        if (JSONObject.class.equals(type)) {
            return responseBody -> {
                try {
                    return new JSONObject(responseBody.string());
                } catch (Exception ex) {
                    return null;
                }
            };
        } else if (String.class.equals(type)) {
            return ResponseBody::string;
        }
        return null;
    }

    public static CustomConverterFactory create() {
        return new CustomConverterFactory();
    }
}