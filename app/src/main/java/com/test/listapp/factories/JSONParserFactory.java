package com.test.listapp.factories;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONParserFactory {

    private static ObjectMapper mapper = new ObjectMapper();

    static {
//        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.OBJECT_AND_NON_CONCRETE);
    }

    public static byte[] writeValue(final Object obj) {
        byte[] data = null;
        try {
            data = mapper.writeValueAsBytes(obj);
        } catch (Exception ignored) {
        }
        return data;
    }

    public static String writeValueAString(final Object obj) {
        String json = null;
        try {
            json = mapper.writeValueAsString(obj);
        } catch (Exception ignored) {
        }
        return json;
    }

    public static <T> T readValue(final byte[] data, final Class<T> type) {
        T t = null;
        try {
            t = mapper.readValue(data, type);
        } catch (Exception ignored) {
        }
        return t;
    }

    public static <T> T readValue(final byte[] data, final TypeReference<T> typeReference) {
        T t = null;
        try {
            t = mapper.readValue(data, typeReference);
        } catch (Exception ignored) {
        }
        return t;
    }

    public static <T> T readValue(final String data, final Class<T> type) {
        T t = null;
        try {
            t = mapper.readValue(data, type);
        } catch (Exception ignored) {
        }
        return t;
    }

    public static <T> T readValue(final String data, final TypeReference<T> typeReference) {
        T t = null;
        try {
            t = mapper.readValue(data, typeReference);
        } catch (Exception ignored) {
        }
        return t;
    }

    public static ObjectMapper getMapper() {
        return mapper;
    }
}
