package com.test.listapp;


import android.app.Application;
import android.content.Context;

public class ListApplication extends Application {

    public static ListApplication get(Context context) {
        return (ListApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
