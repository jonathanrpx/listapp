package com.test.listapp.domain;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppInfo implements Serializable {

    private String id;
    private String bundleId;
    private String name;
    private String title;
    private String contentType;
    private String summary;
    private List<String> images;
    private String rights;
    private String url;
    private Date releaseDate;

    private PriceInfo price;
    private CategoryInfo category;

    public AppInfo() {
    }

    public AppInfo(String id,
                   String bundleId,
                   String name,
                   String title,
                   String contentType,
                   String summary,
                   List<String> images,
                   String rights,
                   String url,
                   Date releaseDate,
                   PriceInfo price,
                   CategoryInfo category) {
        this.id = id;
        this.bundleId = bundleId;
        this.name = name;
        this.title = title;
        this.contentType = contentType;
        this.summary = summary;
        this.images = images;
        this.rights = rights;
        this.url = url;
        this.releaseDate = releaseDate;

        this.price = price;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public String getBundleId() {
        return bundleId;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getContentType() {
        return contentType;
    }

    public String getSummary() {
        return summary;
    }

    public List<String> getImages() {
        return images;
    }

    public String getRights() {
        return rights;
    }

    public String getUrl() {
        return url;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public PriceInfo getPrice() {
        return price;
    }

    public CategoryInfo getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "App{" +
                "id='" + id + '\'' +
                ", bundleId='" + bundleId + '\'' +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", contentType='" + contentType + '\'' +
                ", summary='" + summary + '\'' +
                ", images=" + images +
                ", rights='" + rights + '\'' +
                ", url='" + url + '\'' +
                ", releaseDate=" + releaseDate +
                ", price=" + price +
                ", category=" + category +
                '}';
    }

    public static class Builder {

        private String id;
        private String bundleId;
        private String name;
        private String title;
        private String contentType;
        private String summary;
        private List<String> images;
        private String rights;
        private String url;
        private Date releaseDate;

        private PriceInfo price;
        private CategoryInfo category;

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setBundleId(String bundleId) {
            this.bundleId = bundleId;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Builder setImage(String image) {
            if (this.images == null) this.images = new ArrayList<>();
            this.images.add(image.trim());
            return this;
        }

        public Builder setImages(String... images) {
            for (String image : images)
                setImage(image);
            return this;
        }

        public Builder setSummary(String summary) {
            this.summary = summary;
            return this;
        }

        public Builder setRights(String rights) {
            this.rights = rights;
            return this;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setReleaseDate(Date releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        public Builder setPrice(PriceInfo price) {
            this.price = price;
            return this;
        }

        public Builder setCategory(CategoryInfo category) {
            this.category = category;
            return this;
        }

        public AppInfo build() {
            return new AppInfo(id, bundleId, name, title, contentType, summary, images, rights, url, releaseDate, price, category);
        }
    }
}
