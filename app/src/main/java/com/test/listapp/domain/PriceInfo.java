package com.test.listapp.domain;


import java.io.Serializable;

public class PriceInfo implements Serializable {

    private double amount;
    private String currency;

    public PriceInfo() {
    }

    public PriceInfo(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Price{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }
}
