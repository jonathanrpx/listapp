package com.test.listapp.domain;


import java.io.Serializable;

public class CategoryInfo implements Serializable {

    private String id;
    private String name;
    private String scheme;

    public CategoryInfo() {
    }

    public CategoryInfo(String id) {
        this.id = id;
    }

    public CategoryInfo(String id, String name, String scheme) {
        this.id = id;
        this.name = name;
        this.scheme = scheme;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getScheme() {
        return scheme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryInfo that = (CategoryInfo) o;
        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Category{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", scheme='" + scheme + '\'' +
                '}';
    }

    public static class Builder {
        private String id;
        private String name;
        private String scheme;

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setScheme(String scheme) {
            this.scheme = scheme;
            return this;
        }

        public CategoryInfo build() {
            return new CategoryInfo(id, name, scheme);
        }
    }
}
