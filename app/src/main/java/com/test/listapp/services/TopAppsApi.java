package com.test.listapp.services;

import android.util.Log;

import com.test.listapp.business.TopAppsBI;
import com.test.listapp.domain.AppInfo;
import com.test.listapp.domain.CategoryInfo;
import com.test.listapp.domain.PriceInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import rx.Observable;
import rx.schedulers.Schedulers;

public class TopAppsApi extends CommonApi implements TopAppsBI {

    private TopAppsService service;

    public TopAppsApi(String url, OkHttpClient okHttpClient) {
        super(url, okHttpClient);
    }

    @Override
    protected void init(Retrofit adapter) {
        service = adapter.create(TopAppsService.class);
    }

    @Override
    public Observable<List<AppInfo>> getTopApps() {
        return service.getTopApps()
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .map(response -> {
                    List<AppInfo> list = new ArrayList<>();
                    JSONObject json;
                    try {
                        JSONArray jsonImages, jsonArray = response.getJSONObject(KEY_FEED)
                                .getJSONArray(KEY_ENTRY);
                        Log.i(TopAppsApi.class.getName(),"getTopApps() "+jsonArray.toString());
                        AppInfo.Builder builder;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            json = jsonArray.getJSONObject(i);
                            builder = new AppInfo.Builder()
                                    .setId(json.getJSONObject(KEY_ID).getJSONObject(KEY_ATTR).getString(KEY_IM_ID))
                                    .setBundleId(json.getJSONObject(KEY_ID).getJSONObject(KEY_ATTR).getString(KEY_BUNDLE_ID))
                                    .setName(json.getJSONObject(KEY_NAME).getString(KEY_LABEL))
                                    .setTitle(json.getJSONObject(KEY_TITLE).getString(KEY_LABEL))
                                    .setCategory(new CategoryInfo.Builder()
                                            .setId(json.getJSONObject(KEY_CATEGORY).getJSONObject(KEY_ATTR).getString(KEY_IM_ID))
                                            .setName(json.getJSONObject(KEY_CATEGORY).getJSONObject(KEY_ATTR).getString(KEY_LABEL))
                                            .setScheme(json.getJSONObject(KEY_CATEGORY).getJSONObject(KEY_ATTR).getString(KEY_SCHEME))
                                            .build())
                                    .setPrice(new PriceInfo(json.getJSONObject(KEY_PRICE).getJSONObject(KEY_ATTR).getDouble(KEY_AMOUNT),
                                            json.getJSONObject(KEY_PRICE).getJSONObject(KEY_ATTR).getString(KEY_CURRENCY)))
                                    .setContentType(json.getJSONObject(KEY_CONTENT_TYPE)
                                            .getJSONObject(KEY_ATTR).getString(KEY_LABEL))
                                    .setRights(json.getJSONObject(KEY_RIGHTS).getString(KEY_LABEL))
                                    .setSummary(json.getJSONObject(KEY_SUMMARY).getString(KEY_LABEL))
                                    .setUrl(json.getJSONObject(KEY_LINK).getJSONObject(KEY_ATTR).getString(KEY_HREF))
                                    .setReleaseDate(new SimpleDateFormat("yyyy-MM-dd").parse(json.getJSONObject(KEY_RELEASE_DATE).getString(KEY_LABEL).substring(0, 10)));
                            jsonImages = json.getJSONArray(KEY_IMAGES);
                            for (int j = 0; j < jsonImages.length(); j++) {
                                builder.setImage(jsonImages.getJSONObject(j).getString(KEY_LABEL));
                            }
                            list.add(builder.build());
                        }
                    } catch (Throwable ignored) {
                        Log.e(TopAppsApi.class.getName(), "getTopApps()", ignored);
                    }
                    return list;
                });
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // WebService Methods ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private interface TopAppsService {

        @GET("topfreeapplications/limit=20/json")
        Observable<JSONObject> getTopApps();

    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Parser Methods ...
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static final String KEY_FEED = "feed";
    private static final String KEY_ENTRY = "entry";
    private static final String KEY_LABEL = "label";
    private static final String KEY_NAME = "im:name";
    private static final String KEY_IMAGES = "im:image";
    private static final String KEY_SUMMARY = "summary";
    private static final String KEY_PRICE = "im:price";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_CURRENCY = "currency";
    private static final String KEY_ATTR = "attributes";
    private static final String KEY_CONTENT_TYPE = "im:contentType";
    private static final String KEY_RIGHTS = "rights";
    private static final String KEY_TITLE = "title";
    private static final String KEY_ID = "id";
    private static final String KEY_IM_ID = "im:id";
    private static final String KEY_BUNDLE_ID = "im:bundleId";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_SCHEME = "scheme";
    private static final String KEY_RELEASE_DATE = "im:releaseDate";
    private static final String KEY_LINK = "link";
    private static final String KEY_HREF = "href";

}
