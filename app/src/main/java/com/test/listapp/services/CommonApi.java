package com.test.listapp.services;


import com.test.listapp.factories.CustomConverterFactory;
import com.test.listapp.factories.JSONParserFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public abstract class CommonApi {

    public CommonApi(final String url,
                     final OkHttpClient okHttpClient) {
        init(url, okHttpClient);
    }

    private void init(final String url, final OkHttpClient okHttpClient) {
        this.init(new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(CustomConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(JSONParserFactory.getMapper()))
                .client(okHttpClient)
                .build());
    }

    protected abstract void init(Retrofit adapter);
}
