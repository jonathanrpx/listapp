package com.test.listapp.services;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;
import com.test.listapp.business.ImageBI;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ImageApi implements ImageBI {

    private final Picasso mPicasso;

    public ImageApi(Picasso picasso) {
        this.mPicasso = picasso;
    }

    @Override
    public Observable<Boolean> download(@NonNull
                                        final String uri,
                                        @NonNull
                                        final ImageView target) {

        return Observable
                .create((Subscriber<? super Boolean> subscriber) -> {
                    try {
                        mPicasso.load(uri).into(target, new Callback() {
                            @Override
                            public void onSuccess() {
                                subscriber.onNext(true);
                                subscriber.onCompleted();
                            }

                            @Override
                            public void onError() {
                                subscriber.onNext(false);
                                subscriber.onCompleted();
                            }
                        });
                    } catch (Exception ex) {
                        subscriber.onError(ex);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .doOnUnsubscribe(() -> mPicasso.cancelRequest(target));
    }

    @Override
    public Observable<Bitmap> download(@NonNull
                                       final String url) {
        return Observable
                .create((Subscriber<? super Bitmap> subscriber) -> {
                    try {
                        subscriber.onNext(mPicasso
                                .load(url)
                                .get());
                        subscriber.onCompleted();
                    } catch (Exception ex) {
                        subscriber.onError(ex);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> download(@NonNull
                                        final String url,
                                        @NonNull
                                        final ImageView target,
                                        final boolean inMemoryCache) {
        return Observable
                .create((Subscriber<? super Boolean> subscriber) -> {
                    try {
                        RequestCreator creator =
                                mPicasso.load(url);

                        if (!inMemoryCache)
                            creator.memoryPolicy(MemoryPolicy.NO_CACHE);

                        creator.into(target, new Callback() {
                            @Override
                            public void onSuccess() {
                                subscriber.onNext(true);
                                subscriber.onCompleted();
                            }

                            @Override
                            public void onError() {
                                subscriber.onNext(false);
                                subscriber.onCompleted();
                            }
                        });
                    } catch (Exception ex) {
                        subscriber.onError(ex);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .doOnUnsubscribe(() -> mPicasso.cancelRequest(target));
    }

    @Override
    public Observable<Bitmap> downloadCircle(@NonNull
                                             final String url) {
        return Observable.create((Subscriber<? super Bitmap> subscriber) -> {
            try {
                subscriber.onNext(mPicasso
                        .load(url)
                        .transform(new CircleTransform())
                        .get());
                subscriber.onCompleted();
            } catch (Exception ex) {
                subscriber.onError(ex);
            }
        });
    }

    @Override
    public Observable<Boolean> downloadCircle(@NonNull
                                              final String url,
                                              @NonNull
                                              final ImageView target,
                                              final boolean inMemoryCache) {

        return Observable
                .create((Subscriber<? super Boolean> subscriber) -> {
                    try {
                        RequestCreator creator = mPicasso.load(url);

                        if (!inMemoryCache)
                            creator.memoryPolicy(MemoryPolicy.NO_CACHE);

                        creator.transform(new CircleTransform())
                                .into(target, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        subscriber.onNext(true);
                                        subscriber.onCompleted();
                                    }

                                    @Override
                                    public void onError() {
                                        subscriber.onNext(false);
                                        subscriber.onCompleted();
                                    }
                                });
                    } catch (Exception ex) {
                        subscriber.onError(ex);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .doOnUnsubscribe(() -> mPicasso.cancelRequest(target));
    }

    @Override
    public Observable<Boolean> downloadRounded(@NonNull
                                               final String url,
                                               @NonNull
                                               final ImageView target,
                                               final boolean inMemoryCache) {
        return Observable
                .create((Subscriber<? super Boolean> subscriber) -> {
                    try {
                        RequestCreator creator = mPicasso.load(url);

                        if (!inMemoryCache)
                            creator.memoryPolicy(MemoryPolicy.NO_CACHE);

                        creator.transform(new RoundedTransform(10, 0))
                                .into(target, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        subscriber.onNext(true);
                                        subscriber.onCompleted();
                                    }

                                    @Override
                                    public void onError() {
                                        subscriber.onNext(false);
                                        subscriber.onCompleted();
                                    }
                                });
                    } catch (Exception ex) {
                        subscriber.onError(ex);
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
                .doOnUnsubscribe(() -> mPicasso.cancelRequest(target));
    }


    private class CircleTransform implements Transformation {

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap circleBitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(circleBitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return circleBitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    private class RoundedTransform implements Transformation {

        private final int radius;  // dp
        private final int margin;  // dp

        public RoundedTransform(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            int width, height, x, y;

            if (source.getWidth() >= source.getHeight()) {
                width = (source.getHeight() * 3) / 4;
                height = source.getHeight();
                x = (source.getWidth() - width) / 2;
                y = 0;
            } else {
                width = source.getWidth();
                height = (source.getWidth() * 4) / 3;
                x = 0;
                y = (source.getHeight() - height) / 2;
            }

            Bitmap rectBitmap = Bitmap.createBitmap(source, x, y, width, height);
            if (rectBitmap != source) {
                source.recycle();
            }

            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(rectBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

            Bitmap roundedBitmap = Bitmap.createBitmap(rectBitmap.getWidth(), rectBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(roundedBitmap);
            canvas.drawRoundRect(new RectF(margin, margin, rectBitmap.getWidth() - margin, rectBitmap.getHeight() - margin), radius, radius, paint);

            if (rectBitmap != roundedBitmap) {
                rectBitmap.recycle();
            }
            return roundedBitmap;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }
}
