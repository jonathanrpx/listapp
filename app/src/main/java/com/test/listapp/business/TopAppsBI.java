package com.test.listapp.business;

import com.test.listapp.domain.AppInfo;

import java.util.List;

import rx.Observable;

public interface TopAppsBI {

    Observable<List<AppInfo>> getTopApps();
}
