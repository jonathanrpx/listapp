package com.test.listapp.business;

import android.graphics.Bitmap;
import android.widget.ImageView;

import rx.Observable;


public interface ImageBI {

    Observable<Boolean> download(String url,
                                 ImageView target);

    Observable<Bitmap> download(String url);


    Observable<Boolean> download(String url,
                                 ImageView target,
                                 boolean inMemoryCache);

    Observable<Bitmap> downloadCircle(String url);


    Observable<Boolean> downloadCircle(String url,
                                       ImageView target,
                                       boolean inMemoryCache);

    Observable<Boolean> downloadRounded(String url,
                                        ImageView target,
                                        boolean inMemoryCache);
}
