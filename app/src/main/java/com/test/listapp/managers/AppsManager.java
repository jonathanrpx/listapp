package com.test.listapp.managers;


import android.content.Context;

import com.test.listapp.domain.AppInfo;
import com.test.listapp.domain.CategoryInfo;
import com.test.listapp.sql.AppInfoSQL;
import com.test.listapp.sql.ListAppDB;

import java.util.List;

import rx.Observable;

public class AppsManager {

    public static Observable<List<AppInfo>> findAll(final Context context) {
        return ListAppDB.getSQLOperator(context, AppInfoSQL.class)
                .read()
                .toList();
    }

    public static Observable<List<AppInfo>> findByCategory(final Context context,
                                                           final CategoryInfo category) {
        return ListAppDB.getSQLOperator(context, AppInfoSQL.class)
                .findByCategory(category)
                .toList();
    }

    public static Observable<Boolean> set(final Context context,
                                          final List<AppInfo> apps) {
        return ListAppDB.getSQLOperator(context, AppInfoSQL.class)
                .create(apps);
    }
}
