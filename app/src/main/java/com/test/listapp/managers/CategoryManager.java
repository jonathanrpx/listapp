package com.test.listapp.managers;


import android.content.Context;

import com.test.listapp.domain.CategoryInfo;
import com.test.listapp.sql.CategoryInfoSQL;
import com.test.listapp.sql.ListAppDB;

import java.util.List;

import rx.Observable;

public class CategoryManager {

    public static Observable<List<CategoryInfo>> findAll(Context context) {
        return ListAppDB.getSQLOperator(context, CategoryInfoSQL.class)
                .read()
                .toList();
    }

    public static Observable<Boolean> set(Context context, List<CategoryInfo> categories) {
        return ListAppDB.getSQLOperator(context, CategoryInfoSQL.class)
                .create(categories);
    }
}
