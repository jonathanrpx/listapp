package com.test.listapp.components;

import com.test.listapp.activities.DefaultActivity;
import com.test.listapp.activities.MainActivity;
import com.test.listapp.activities.SummaryAppActivity;
import com.test.listapp.modules.NetworkApiModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkApiModule.class})
public interface NetworkComponent {

    void inject(MainActivity activity);

    void inject(SummaryAppActivity activity);

    void inject(DefaultActivity activity);
}
